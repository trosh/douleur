/* Douleur, par John Gliksberg, en 2018 */

function alterneVisibilite(div)
{
	if (div.style.display == "block")
	{
		div.style.display = "none";
		div.getElementsByClassName("actif")[0].checked = false;
	}
	else
	{
		div.style.display = "block";
		div.getElementsByClassName("actif")[0].checked = true;
		div.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
	}
}

function recto(rectoverso)
{
	const fieldset = rectoverso.parentNode.parentNode;
	fieldset.getElementsByClassName("recto")[0].style.display = "block";
	fieldset.getElementsByClassName("verso")[0].style.display = "none";
	rectoverso.innerText = "Voir le verso";
	rectoverso.onclick = function() { verso(rectoverso); };
}

function verso(rectoverso)
{
	const fieldset = rectoverso.parentNode.parentNode;
	fieldset.getElementsByClassName("recto")[0].style.display = "none";
	fieldset.getElementsByClassName("verso")[0].style.display = "block";
	rectoverso.innerText = "Voir le recto";
	rectoverso.onclick = function() { recto(rectoverso); };
}

/* IE-compatible localStorage cookie-based reimplementation
 *
 * From https://developer.mozilla.org/en-US/docs/Web/API/Storage/LocalStorage
 */

if (!window.localStorage) {
	Object.defineProperty(window, "localStorage", new (function () {
		var aKeys = [], oStorage = {};
		Object.defineProperty(oStorage, "getItem", {
			value: function (sKey) { return sKey ? this[sKey] : null; },
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(oStorage, "key", {
			value: function (nKeyId) { return aKeys[nKeyId]; },
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(oStorage, "setItem", {
			value: function (sKey, sValue) {
				if(!sKey) { return; }
				document.cookie = escape(sKey) + "=" + escape(sValue) + "; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/";
			},
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(oStorage, "length", {
			get: function () { return aKeys.length; },
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(oStorage, "removeItem", {
			value: function (sKey) {
				if(!sKey) { return; }
				document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
			},
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(oStorage, "clear", {
			value: function () {
				if(!aKeys.length) { return; }
				for (var sKey in aKeys) {
					document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
				}
			},
			writable: false,
			configurable: false,
			enumerable: false
		});
		this.get = function () {
			var iThisIndx;
			for (var sKey in oStorage) {
				iThisIndx = aKeys.indexOf(sKey);
				if (iThisIndx === -1) { oStorage.setItem(sKey, oStorage[sKey]); }
				else { aKeys.splice(iThisIndx, 1); }
				delete oStorage[sKey];
			}
			for (aKeys; aKeys.length > 0; aKeys.splice(0, 1)) { oStorage.removeItem(aKeys[0]); }
			for (var aCouple, iKey, nIdx = 0, aCouples = document.cookie.split(/\s*;\s*/); nIdx < aCouples.length; nIdx++) {
				aCouple = aCouples[nIdx].split(/\s*=\s*/);
				if (aCouple.length > 1) {
					oStorage[iKey = unescape(aCouple[0])] = unescape(aCouple[1]);
					aKeys.push(iKey);
				}
			}
			return oStorage;
		};
		this.configurable = false;
		this.enumerable = true;
	})());
}

/* IE-compatible way to read radio element's .value */
function getRadioValue(radio)
{
	if (radio.value !== undefined) return radio.value;
	for (var i = 0; i < radio.length; ++i)
		if (radio[i].checked)
			return radio[i].value;
}

function enregistrer()
{
	const maintenant = Date.now();
	const formelts = this.form.elements;
	const patient = formelts["patient"].value;
	if (patient.length == 0)
	{
		alert("Il faut un nom de patient");
		document.getElementById("patient_fieldset").scrollIntoView(
			{behavior: "smooth", block: "start", inline: "nearest"});
		return;
	}
	enregistrement = {patient: patient};
	var aucun_choix = true;
	if (formelts["eva_actif"].checked
	 && formelts["eva_modifie"].checked)
	{
		aucun_choix = false;
		enregistrement.eva = parseInt(formelts["eva"].value);
	}
	if (formelts["fpa_actif"].checked
	 && formelts["fpa"].value !== "")
	{
		aucun_choix = false;
		enregistrement.fpa = parseInt(formelts["fpa"].value);
	}
	if (formelts["evendol_actif"].checked)
	{
		const parties_evendol = ["expression", "mimique", "mouvements", "positions", "environnement"];
		if (parties_evendol.map(
		    function(partie) {
		    	return getRadioValue(formelts["evendol_" + partie]) !== "";
		    }).reduce(function(acc, val) { return acc && val; }))
		{
			aucun_choix = false;
			enregistrement.evendol = {
				douleur: parties_evendol.map(
					function(partie) {
						return parseInt(getRadioValue(formelts["evendol_" + partie]));
					}
				).reduce(function(acc, val) { return acc + val; }),
				meta: getRadioValue(formelts["evendol_meta"])
			};
		}
	}
	if (formelts["flacc_actif"].checked)
	{
		const parties_flacc = ["visage", "jambes", "activite", "cris", "consolabilite"];
		if (parties_flacc.map(
		    	function(partie) {
		    		return getRadioValue(formelts["flacc_" + partie]) !== "";
		    	}
		    ).reduce(function(acc, val) { return acc && val; }))
		{
			aucun_choix = false;
			const douleur = parties_flacc.map(
				function(partie) {
					return parseInt(getRadioValue(formelts["flacc_" + partie]));
				}
			).reduce(function(acc, val) { return acc + val; });
			if (formelts["flacc_meta"].value !== "")
				enregistrement.flacc = {
					douleur: douleur,
					meta: formelts["flacc_meta"].value
				};
			else
				enregistrement.flacc = douleur;
		}
	}
	if (aucun_choix)
	{
		alert("Aucune evaluation effectuée");
		return;
	}
	console.log(enregistrement);
	enregistrement_texte = JSON.stringify(enregistrement);
	window.localStorage.setItem(new String(maintenant), enregistrement_texte);
	window.location.href = "dernier.html";
}

function recherche_patients() {
	document.getElementById("patients").innerHTML = "";
	const requete = this.form.elements["patient_recherche_requete"].value.toLowerCase();
	document.getElementById("patient_recherche_requete").value = "";
	var precedents = [];
	var match = false;
	for (i = 0; i < window.localStorage.length; ++i)
	{
		const enregistrement = JSON.parse(window.localStorage.getItem(window.localStorage.key(i)));
		if (precedents.includes(enregistrement.patient))
			continue
		if (enregistrement.patient.toLowerCase().match(requete) === null)
			continue;
		match = true;
		var bouton = document.createElement("button");
		bouton.textContent = enregistrement.patient;
		bouton.onclick = function() {
			document.getElementById("patient").value = enregistrement.patient;
			document.getElementById("patients").innerHTML = "";
		}
		document.getElementById("patients").appendChild(bouton);
		precedents.push(enregistrement.patient);
	}
	if (match === false)
	{
		document.getElementById("patients").appendChild(
			document.createTextNode("Aucun patient ne correspond à la recherche"));
	}
}

function index_load()
{
	var requete = document.getElementById("patient_recherche_requete");
	requete.onkeyup = function(event) {
		if (event.key === "Enter")
			recherche_patients.bind(requete)();
	};
	document.getElementById("patient_recherche").onclick = recherche_patients;
	Array.prototype.forEach.call(
		document.getElementById("toggles").childNodes,
		function(toggle) {
			toggle.onclick = function() {
				alterneVisibilite(document.getElementById(toggle.id.slice(7) + "box"));
			};
		}
	);
	Array.prototype.forEach.call(
		document.getElementsByClassName("visibilite"),
		function(visibilite) {
			const legend = visibilite.getElementsByTagName("legend")[0];
			legend.appendChild(document.createTextNode(" "));
			const rectoverso = document.createElement("button");
			rectoverso.innerText = "Voir le verso";
			rectoverso.onclick = function() { verso(rectoverso); };
			legend.appendChild(rectoverso);
		}
	);
	var eva = document.getElementById("evabox");
	eva.getElementsByClassName("recto")[0]
	   .getElementsByTagName("input")[0].onchange = function() {
		eva.getElementsByClassName("modifie")[0].checked = true;
	}
	document.getElementById("enregistrer").onclick = enregistrer;
}

const EVALUATIONS = {
	eva:     {max: "10", seuil: 3},
	fpa:     {max: "10", seuil: 4},
	evendol: {max: "15", seuil: 4},
	flacc:   {max: "10", seuil: 4}
};

function display_eval(temps_int, enregistrement)
{
	var li = document.createElement("fieldset");
	const temps = new Date(temps_int);
	var temps_legend = document.createElement("legend");
	temps_legend.setAttribute("class", "temps");
	temps_legend.textContent = temps.toLocaleDateString("fr-FR") + " "
		+ temps.toTimeString().slice(0, 5) + " ";
	var bouton = document.createElement("button");
	bouton.setAttribute("class", "supprimer");
	bouton.onclick = (function() {
		var temps_cpy = temps_int;
		return function () {
			window.localStorage.removeItem(new String(temps_cpy));
			window.location.reload();
		};
	})();
	temps_legend.appendChild(bouton);
	li.appendChild(temps_legend);
	Object.keys(enregistrement).forEach(function(evaluation) {
		if (evaluation === "patient") return;
		const eval = enregistrement[evaluation];
		if (eval !== undefined)
		{
			li.appendChild(document.createTextNode(evaluation + "="));
			const evaltext = document.createElement("b");
			var douleur = undefined;
			const max = EVALUATIONS[evaluation].max;
			if (typeof eval === "number")
			{
				douleur = eval;
				evaltext.textContent = new String(douleur) + "/" + max;
			}
			else
			{
				douleur = eval.douleur;
				evaltext.textContent = new String(douleur)
					+ "/" + max + "(" + eval.meta + ")";
			}
			li.appendChild(evaltext);
			if (douleur >= EVALUATIONS[evaluation].seuil)
			{
				var ttt = document.createElement("img");
				ttt.setAttribute("class", "ttt");
				ttt.setAttribute("src", "https://emojipedia-us.s3.amazonaws.com/thumbs/120/samsung/137/pill_1f48a.png");
				li.appendChild(ttt);
			}
			li.appendChild(document.createTextNode("  "));
		}
	});
	return li;
}

function dernier_load()
{
	if (window.localStorage.length == 0)
	{
		document.getElementById("nom_patient").parentNode.textContent = "Aucune évaluation";
		return;
	}
	var tous_temps = [];
	for (i = 0; i < window.localStorage.length; ++i)
		tous_temps.push(parseInt(window.localStorage.key(i)));
	tous_temps.sort();
	max_temps = tous_temps[tous_temps.length - 1];
	dernier_enregistrement_json = window.localStorage.getItem(new String(max_temps));
	dernier_enregistrement = JSON.parse(dernier_enregistrement_json);
	dernier_patient = dernier_enregistrement.patient;
	document.getElementById("nom_patient").textContent = dernier_patient;
	var enregistrements = document.getElementById("evaluations");
	while (tous_temps.length > 0)
	{
		temps_decroissant = tous_temps.pop();
		enregistrement_json = window.localStorage.getItem(new String(temps_decroissant));
		enregistrement = JSON.parse(enregistrement_json);
		if (enregistrement.patient != dernier_patient)
			continue;
		enregistrements.appendChild(display_eval(temps_decroissant, enregistrement));
	}
}

function toutsupprimer()
{
	if (window.confirm("Êtes-vous sûr de vouloir supprimer définitivement toutes les évaluations ?"))
	{
		for (var i = localStorage.length - 1; i >= 0; --i)
		{
			localStorage.removeItem(localStorage.key(i));
		}
		window.location.reload();
	}
}

function tous_load()
{
	if (window.localStorage.length == 0)
	{
		document.getElementById("evaluations").textContent = "Aucune évaluation";
	}
	else
	{
		var tous_temps = [];
		for (i = 0; i < window.localStorage.length; ++i)
		{
			tous_temps.push(parseInt(window.localStorage.key(i)));
		}
		tous_temps.sort();
		while (tous_temps.length > 0)
		{
			premier_temps = tous_temps[tous_temps.length - 1];
			enregistrement_json = window.localStorage.getItem(new String(premier_temps));
			enregistrement = JSON.parse(enregistrement_json);
			const patient = enregistrement.patient;
			var titre_patient = document.createElement("h2");
			titre_patient.setAttribute("class", "nom_patient");
			titre_patient.textContent = patient;
			document.getElementById("container").appendChild(titre_patient);
			var enregistrements_patient = document.createElement("div");
			enregistrements_patient.setAttribute("class", "evaluations_patient");
			for (j = tous_temps.length - 1; j >= 0; --j)
			{
				const temps_decroissant = tous_temps[j];
				enregistrement_json = window.localStorage.getItem(new String(temps_decroissant));
				enregistrement = JSON.parse(enregistrement_json);
				if (enregistrement.patient != patient)
					continue;
				tous_temps.splice(j, 1);
				enregistrements_patient.appendChild(display_eval(temps_decroissant, enregistrement));
			}
			document.getElementById("container").appendChild(enregistrements_patient);
		}
	}
	document.getElementById("toutsupprimer").onclick = toutsupprimer;
}